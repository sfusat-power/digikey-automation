import requests
import time
import json

class DigikeyClient:

	def __init__(self, client_id, client_secret):
		self.client_id = client_id
		self.client_secret = client_secret

	def get_api_token(self):

		token = None
		try:
			with open("digikey-client-token.json", "r") as token_file:
				token = json.load(token_file)
		except:
			pass

		if token == None or "value" not in token or "expires" not in token:
			token = {"value": "", "expires": 0}

		if time.time() >= token["expires"]:
			print("Token expired.")
			while True:
				# go to https://sso.digikey.com/as/authorization.oauth2?response_type=code&client_id=bec7e2f4-fbd9-4bee-aa33-f2ae66b6c51d&redirect_uri=https://localhost
				# enter digikey user credentials. You will be redirected to a "site can't be reached" message. The url will looks something like https://localhost/?code=nrYBaVhUlPxnX3ERssRctRH2VJJrw6WgNqOHxQAE
				# paste the code from the url as your authorization code. nrYBaVhUlPxnX3ERssRctRH2VJJrw6WgNqOHxQAE in the example url.
				authorization_code = input("New authorization code: ")

				url = "https://sso.digikey.com/as/token.oauth2"
				r = requests.post(url, headers={"content-type": "application/x-www-form-urlencoded"}, data={"code": authorization_code, "client_id": self.client_id, "client_secret": self.client_secret, "redirect_uri":"https://localhost", "grant_type":"authorization_code"})
				if "access_token" not in r.json():
					print("Authorization code invalid or already used.")
					continue
				token["value"] = r.json()["access_token"]
				token["expires"] = time.time() + r.json()["expires_in"]
				with open("digikey-client-token.json", "w") as token_file:
					json.dump(token, token_file)
				break

		return token

	def get_authorization_headers(self):
		headers = {
		    "x-ibm-client-id": self.client_id,
		    "authorization": self.get_api_token()["value"],
		    "content-type": "application/json",
		    "accept": "application/json"
		    }
		return headers

	def get_part_details(self, part):

		url = "https://api.digikey.com/services/partsearch/v2/partdetails"
		r = requests.post(url, json={ "Part": part, "IncludeAllAssociatedProducts": False,"IncludeAllForUseWithProducts": False}, headers=self.get_authorization_headers())
		return r.json()

	def keyword_search(self, keywords):

		url = "https://api.digikey.com/services/partsearch/v2/keywordsearch"
		r = requests.post(url, json={"SearchOptions":["RoHSCompliant"],"Keywords":keywords,"RecordCount":"10","RecordStartPosition":"0","Filters":{"ParametricFilters":[{"ParameterId":"2085","Value":"100 Ohms"}]},"Sort":{"Option":"SortByUnitPrice","Direction":"Ascending","SortParameterId":"50"},"RequestedQuantity":"50"}, headers=self.get_authorization_headers())
		return r.json()